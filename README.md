# covid19-loss-analysis

Simulations around lost revenue due to the economic fallout of the covid-19 crisis.
We render two apps and control them with two buttons inside streamlit:
- An app where you can put recovery parameters and tweak them
or input several pre-defined scenarios (defined on input_parameters.py file)
for your your sales data - Called Parameterized app.
- An app where you can model pre-computed scenarios per each dimension
(for example: different regions, different products, etc.) - Called Dimensions App.

# open streamlit app

streamlit run app/app.py

# Notes:

On the app you can customize the parameters yourself or choose a specific scenario.
Scenarios are set according to virus spread on the community and economic recovery.

You can add specific scenarios to the dictionary in input_parameters.py. These
scenarios will then be read by the dropdowns in streamlit app.

Industry logic is still high level only with weights per industry - we are
still adding other functionalities based on the research available.

Find an example of the PDF report generated for dimensions at the /report_example
folder - the report is in portuguese.