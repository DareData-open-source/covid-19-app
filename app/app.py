import streamlit as st
from src import (
    build_intro,
    app_parametrized,
    app_dimensions,
    helper_functions
)


def build_app():
    '''
    Builds streamlit app.
    '''
    # Build intro
    build_intro.write_header()

    session_state = helper_functions.SessionState.get(
        name="",
        button_sent=None
        )

    button_sent = st.button('Render Parametrized App')

    button_dimensions = st.button('Render Dimensions App')

    # Control flow of session state between no app rendered
    # Parametrized app or dimension app
    if button_sent:
        session_state.button_sent = True
    elif button_dimensions:
        session_state.button_sent = False

    if session_state.button_sent:
        app_parametrized.build_parametrized_app()
    elif session_state.button_sent is False:
        app_dimensions.build_app_dimensions()
    else:
        st.markdown('**Click above on the app you want to render.**')


if __name__ == "__main__":
    build_app()
