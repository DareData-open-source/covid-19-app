from src import sidebar
import recoverymodel as rm
import streamlit as st
from typing import Callable
from src import (
    input_parameters
)


def generate_scenario_params_sidebar(
    lockdown_numbers: int,
    lockdown_params: Callable[[int], None],
    recovery_params: Callable[[int], None],
):
    '''
    Generates lockdown and recovery objects
    coming from the parameters of the sidebar of streamlit app.
    '''

    lockdown_objects = []
    recovery_objects = []

    for lockdown in list(range(1, lockdown_numbers+1)):
        p1, p2, p3, p4, p5, p6, p7 = lockdown_params(lockdown)

        lockdown_obj = {
            'start': p1,
            'length': p2,
            'immediate_loss': p3,
            'client_deaths': rm.generic_linear_func(p4, p5),
            'lockdown_growth': rm.generic_linear_func(p6, p7)
        }

        r1, r2, r3, r4 = recovery_params(lockdown)

        recovery_obj = {
            'client_recovery': rm.generic_linear_func(r1, r2),
            'market_appetite': rm.generic_linear_func(r3, r4)
        }

        lockdown_objects.append(lockdown_obj)
        recovery_objects.append(recovery_obj)

    return lockdown_objects, recovery_objects


def generate_scenario_params(
    lockdowns: dict,
    recoveries: dict,
    dimensions=False
):
    '''
    Generates parameters for specific scenario
    '''

    lockdown_objects = []
    recovery_objects = []

    for index, _ in enumerate(lockdowns):

        lockdown = lockdowns['Lockdown_'+str(index+1)]

        lockdown_obj = {
            'start': lockdown['Lockdown Start'],
            'length': lockdown['Lockdown Length'],
            'immediate_loss': lockdown['Initial Drop'],
            'client_deaths': rm.generic_linear_func(
                lockdown['Lockdown Death Ratio Intercept'],
                lockdown['Lockdown Death Ratio Slope']
                ),
            'lockdown_growth': rm.generic_linear_func(
                lockdown['Lockdown Growth Intercept'],
                lockdown['Lockdown Growth Slope']
                )
        }

        recovery = recoveries['Recovery_'+str(index+1)]

        recovery_obj = {
            'client_recovery': rm.generic_linear_func(
                recovery['Recovery New Clients Intercept'],
                recovery['Recovery New Clients Slope']
                ),
            'market_appetite': rm.generic_linear_func(
                recovery['Recovery Growth Intercept'],
                recovery['Recovery Growth Slope']
                )
        }

        # If we are using parameterized app we want to render
        # scenarios on the sidebar - otherwise we will just keep
        # custom scenarios

        if not(dimensions):

            # Create new parameters based
            p1, p2, p3, f1, f2, f3, f4 = (
                sidebar.scenario_update_params(
                    index,
                    lockdown,
                    recovery
                )
            )

            # Update scenario if user changes sidebar

            lockdown_obj['start'] = p1
            lockdown_obj['length'] = p2
            lockdown_obj['immediate_loss'] = p3
            lockdown_obj['client_deaths'] = f1
            lockdown_obj['lockdown_growth'] = f2
            recovery_obj['client_recovery'] = f3
            recovery_obj['market_appetite'] = f4

        lockdown_objects.append(lockdown_obj)
        recovery_objects.append(recovery_obj)

    return lockdown_objects, recovery_objects


def default_scenario():
    '''
    Configure default scenario
    '''

    # Get number of lockdowns
    lockdown_numbers = sidebar.get_lockdown_numbers()

    lockdowns, recoveries = generate_scenario_params_sidebar(
        lockdown_numbers,
        sidebar.lockdown_parameters,
        sidebar.recovery_parameters
        )

    return lockdowns, recoveries


def specific_scenario(
    lock_params: list,
    recovery_params: list,
    sidebar=False
):
    lockdowns, recoveries = generate_scenario_params(
        lock_params,
        recovery_params,
        sidebar
        )
    return lockdowns, recoveries


def scenario_set(
    dimensions=False,
    iterator=0
):
    '''
    Sets scenario based on user input
    on the select boxes from streamlit app
    '''

    scenarios = input_parameters.read_scenarios()

    # if dimensions render on sidebar instead of body

    if dimensions:
        # We need an iterator if we are rendering on the sidebar (dimension)
        # app as streamlit can't have the same similar objects with the same
        # key - in the parametrized app we only render this select box once
        # so the iterator can be 0 everytime
        scenario = st.sidebar.selectbox(
            "Select Scenario",
            [*scenarios],
            key=iterator
        )
    else:
        scenario = st.selectbox(
            "Select Scenario",
            [*scenarios]
        )

    # If user left --general on industries and -- custom in scenarios
    # we will let him parametrize the sidebar himself

    s = scenarios[scenario]
    lockdowns = (
        {k: v for k, v in s.items() if k.startswith('Lockdown')}
    )

    recoveries = (
        {k: v for k, v in s.items() if k.startswith('Recovery')}
    )

    if scenario == '--custom' and not(dimensions):
        lockdown_objects, recovery_objects = default_scenario()
    else:
        # Read specific scenarios
        lockdown_objects, recovery_objects = specific_scenario(
            lockdowns,
            recoveries,
            dimensions
        )

    return lockdown_objects, recovery_objects, scenario


def set_industry_weight():

    industries = input_parameters.read_industries()

    # User can select the industry on the select boxes below
    industry = st.selectbox(
        "Select your Industry",
        [*industries]
    )

    return industries[industry]
