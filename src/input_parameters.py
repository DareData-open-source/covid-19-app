def read_scenarios() -> dict:
    '''
    Reads scenarios and outputs them
    into a dictionary
    '''

    scenarios = {
        "--custom": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 8,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0,
                "Lockdown Death Ratio Slope": 0,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0
            },
            "Description_PT": """Seleccionou um cenário customizado.""",
            "Description_EN": """You have selected a custom scenario."""
        },
        "Virus Contained + Slow Economic Recovery": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.2,
                "Lockdown Death Ratio Slope": 0.01,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Description_PT": """Fraca recuperação económica e vírus contido.
            Neste cenário iremos apenas ter um período de estado de emergência
            mas perspectiva-se uma fraca recuperação económica. Alguns negócios
            irão desaparecer e não serão substituídos por outros sendo que a
            confiança dos consumidores irá voltar lentamente. Com este cenário,
            perspectiva-se uma recuperação total apenas no fim de 2021.""",
            "Description_EN": """Slow eonomic recovery and virus contained.
            In this scenario we will only have a lockdown period and there
            is the expectation of a weak economic recovery. Some businesses
            will close and will not be substituted by others while the
            consumer confidence will return slowly. With this scenario,
            the full recovery of the economy is expected by the end of 2021."""
        },
        "Virus Resurges + Slow Economic Recovery": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.1,
                "Lockdown Death Ratio Slope": 0,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.01
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Lockdown_2": {
                "Lockdown Start": 30,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0,
                "Lockdown Death Ratio Slope": 0,
                "Lockdown Growth Intercept": 0.01,
                "Lockdown Growth Slope": 0.01
            },
            "Recovery_2": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0.02,
                "Recovery Growth Slope": 0.03
            },
            "Description_PT": """Fraca recuperação económica e vírus a
            ressurgir no fim de 2020.
            Neste cenário iremos ter mais do que um período de estado de
            emergência e perspectiva-se uma fraca recuperação económica.
            Irão desaparecer negócios tanto na primeira como na segunda vaga
            e a confiança dos consumidores irá voltar lentamente.
            Com este cenário, perspectiva-se uma recuperação total depois
            de 2021.""",
            "Description_EN": """Weak economic recovery and virus ressurges at
            the end of 2020. In this scenario we will have more than one
            lockdown period and there is an expectation of a week economic
            recovery. Some businesses will disappear on the several waves
            of the pandemic and consumer confidence will return slowly.
            With these parameters, the full economic recovery will only
            return after 2021."""
            },
        "Virus Uncontroled + Slow Economic Recovery": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 15,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.1,
                "Lockdown Death Ratio Slope": 0.02,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Lockdown_2": {
                "Lockdown Start": 30,
                "Lockdown Length": 15,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0,
                "Lockdown Death Ratio Slope": 0,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0
            },
            "Recovery_2": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0.01,
                "Recovery Growth Slope": 0.01
            },
            "Description_PT": """Fraca recuperação económica e vírus com
            várias vagas até ao fim de 2021.
            Neste cenário iremos ter mais do que um período de estado de
            emergência e perspectiva-se uma fraca recuperação económica.
            A confiança dos consumidores irá demorar muito tempo até atingir
            níveis que permitam uma recuperação económica estável.
            Com este cenário, não existe previsão de remota de actividade
            económica normal.""",
            "Description_EN": """Weak economic recovery and several virus
            peaks until the end of 2021.
            With this scenario we will have multiple lockdown periods and there
            is an expectation of a week economic recovery.
            Consumer confidence will take a lot of time until it reaches
            an acceptable level that will boost the economic recovery
            With this scenario, there isn't a clear perspective about
            full economic recovery"""
            },
        "Virus Contained + Mid Economic Recovery": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.02,
                "Lockdown Death Ratio Slope": 0.02,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.01
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0.02,
                "Recovery New Clients Slope": 0.02,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Description_PT": """Vírus contido e recuperação económica
            média com efeito ascendente.
            Neste cenário iremos ter apenas um período de estado de
            emergência e perspectiva-se uma média recuperação económica.
            A confiança dos consumidores irá demorar algum tempo a recuperar.
            Com este cenário, a economia deverá retornar ao normal no segundo
            trimestre de 2021.""",
            "Description_EN": """ Virus contained and average economic recovery
            with ascending slope.
            With this scenario we will have only one lockdown period. Consumer
            confidence will take some time to recovery and full economic
            recovery will happen around Q2 2021."""
            },
        "Virus Resurges + Mid Economic Recovery": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.02,
                "Lockdown Death Ratio Slope": 0.02,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.01
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0.02,
                "Recovery New Clients Slope": 0.02,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Lockdown_2": {
                "Lockdown Start": 30,
                "Lockdown Length": 8,
                "Initial Drop": 0.7,
                "Lockdown Death Ratio Intercept": 0,
                "Lockdown Death Ratio Slope": 0,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0
            },
            "Recovery_2": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0.1,
                "Recovery Growth Slope": 0.02
            },
            "Description_PT": """Vírus a ressurgir e recuperação económica
            média com efeito ascendente.
            Neste cenário iremos ter mais do que um período de estado de
            emergência e perspectiva-se uma média recuperação económica.
            A confiança dos consumidores irá demorar algum tempo a recuperar e
            será afectada pelo segundo estado de emergência, causando maior
            impacto económico.
            Com este cenário, a economia deverá retomar ao normal no terceiro
            trimestre de 2021.""",
            "Description_EN": """Virus resurges and there will be an
            average economic recovery.
            With this scenario we will have more than one lockdown period
            and there is an expectation of some economic recovery.
            Consumer confidence will take some time to recover and will
            be affected by a second lockdown period - causing some economic
            impact. With this scenario, economy will return to normal
            around Q3 2021."""
            },
        "Virus Uncontroled + Mid Economic Recovery": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.02,
                "Lockdown Death Ratio Slope": 0.02,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.01
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0.02,
                "Recovery New Clients Slope": 0.01,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Lockdown_2": {
                "Lockdown Start": 30,
                "Lockdown Length": 8,
                "Initial Drop": 0.7,
                "Lockdown Death Ratio Intercept": 0,
                "Lockdown Death Ratio Slope": 0,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0
            },
            "Recovery_2": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0.09,
                "Recovery Growth Slope": 0.00
            },
            "Description_PT": """Vírus com mais do que uma vaga
            e recuperação económica média com efeito ascendente.
            Neste cenário iremos ter diversos períodos de estado de
            emergência e perspectiva-se uma média recuperação económica.
            A confiança dos consumidores irá demorar bastante tempo a recuperar
            e será afectada pelos vários estados de emergência, causando um
            enorme impacto económico.
            Com este cenário, a economia deverá retomar ao normal no quarto
            trimestre de 2021.""",
            "Description_EN": """Virus with several peaks and mid economic
            recovery.
            With this scenario we will have several lockdown periods but
            there is a perspective of a mid economic recovery.
            Consumer confidence will take quite a while to catch up and
            will be affected by the several lockdown periods, having a
            huge impact on the economy. Nevertheless, population and economic
            adaptation will make an economic recovery possible.
            With this scenario, the expectation is that a fully economic
            recovery will happen around Q4 2021."""
            },
        "Virus Contained + Fast Economic Recovery": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.02,
                "Lockdown Death Ratio Slope": 0.02,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.01
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0.02,
                "Recovery New Clients Slope": 0.02,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.03
            },
            "Description_PT": """Rápida recuperação económica e vírus contido.
            Neste cenário iremos apenas ter um período de estado de emergência
            e perspectiva-se uma rápida recuperação económica. Alguns negócios
            irão desaparecer mas serão substituídos por outros e a confiança
            dos consumidores irá voltar rapidamente. Com este cenário,
            perspectiva-se uma recuperação total entre o quarto trimestre de
            2020 e o primeiro trimestre de 2021.""",
            "Description_EN": """Virus contained and fast economic recovery.
            With this scenario, we will only have one lockdown period and
            there is an expectation of a fast economic recovery. Some
            businesses will close but will be quickly replaced by new ones.
            With this scenario, there is an expectation of a full economic
            recovery between Q4 2020 and Q1 2021."""
            },
        "Virus Resurges + Fast Economic Recovery": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.02,
                "Lockdown Death Ratio Slope": 0.02,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.01
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0.02,
                "Recovery New Clients Slope": 0.02,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Lockdown_2": {
                "Lockdown Start": 30,
                "Lockdown Length": 8,
                "Initial Drop": 0.6,
                "Lockdown Death Ratio Intercept": 0,
                "Lockdown Death Ratio Slope": 0,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0
            },
            "Recovery_2": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0.2,
                "Recovery Growth Slope": 0.02
            },
            "Description_PT": """Rápida recuperação económica e vírus a
            ressurgir mais do que uma vez.
            Neste cenário iremos apenas ter dois períodos de estado de
            emergência e perspectiva-se uma rápida recuperação económica.
            Alguns negócios irão desaparecer mas serão substituídos por outros
            e a confiança dos consumidores irá adaptar-se rapidamente a uma
            nova realidade. Com este cenário, perspectiva-se uma recuperação
            total no primeiro ou segundo trimestre de 2021.""",
            "Description_EN": """Quick economic recovery and multiple
            virus peaks.
            With this scenario we will have only two lockdown periods and
            there is an expectation towards a quick economic recovery.
            Some businesses will close but will be replaced by others
            and consumer confidence will rapidly adapt to the new reality.
            With this scenario, there is an expectation of a full economic
            recovery on Q1 or Q2 of 2021."""
            },
        "Virus Uncontroled + Fast Economic Recovery": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.02,
                "Lockdown Death Ratio Slope": 0.02,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.01
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0.02,
                "Recovery New Clients Slope": 0.02,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Lockdown_2": {
                "Lockdown Start": 30,
                "Lockdown Length": 8,
                "Initial Drop": 0.65,
                "Lockdown Death Ratio Intercept": 0,
                "Lockdown Death Ratio Slope": 0,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0
            },
            "Recovery_2": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0.15,
                "Recovery Growth Slope": 0.01
            },
            "Description_PT": """Rápida recuperação económica e vírus a
            com diversos picos.
            Neste cenário iremos ter mais do que dois períodos de estado de
            emergência mas perspectiva-se uma rápida recuperação económica.
            A capacidade de adaptação dos consumidores e das empresas irá
            superar os períodos de estado de emergência e permitirá uma
            recuperação estável. Com este cenário, perspectiva-se uma
            recuperação total no terceiro trimestre de 2021."""
            },
        "Virus Resurges + Mid Economic Recovery - with Lockdown Growth": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.02,
                "Lockdown Death Ratio Slope": 0.02,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.04
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0.02,
                "Recovery New Clients Slope": 0.02,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Lockdown_2": {
                "Lockdown Start": 30,
                "Lockdown Length": 8,
                "Initial Drop": 0.7,
                "Lockdown Death Ratio Intercept": 0,
                "Lockdown Death Ratio Slope": 0,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.04
            },
            "Recovery_2": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0.1,
                "Recovery Growth Slope": 0.02
            },
            "Description_PT": """Vírus a ressurgir e recuperação económica
            média com efeito ascendente.
            Neste cenário iremos ter mais do que um período de estado de
            emergência e perspectiva-se uma média recuperação económica.
            A confiança dos consumidores irá demorar algum tempo a recuperar e
            será afectada pelo segundo estado de emergência, causando maior
            impacto económico. Durante os períodos de estado de emergência
            existe a expectativa de algum crescimento por adaptação do negócio.
            Com este cenário, a economia deverá retomar ao normal no terceiro
            trimestre de 2021.""",
            "Description_EN": """Virus with multiple peaks and mid
            economic recovery.
            With this scenario, we will have more than one lockdown period
            and there is an expectation of a mid economic recovery.
            Consumer confidence will take some time to catch up and will
            be affected by a second lockdown period. During lockdown moments
            there is an expectation of some business growth due to
            business adaptation.
            With this scenario, there is an expectation of a full economic
            recovery on Q3 2021."""
            },
        "Virus Contained + Slow Economic Recovery with Lockdown Growth": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.2,
                "Lockdown Death Ratio Slope": 0.01,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.03
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0,
                "Recovery New Clients Slope": 0,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Description_PT": """Fraca recuperação económica e vírus contido.
            Neste cenário iremos apenas ter um período de estado de emergência
            mas perspectiva-se uma fraca recuperação económica. Alguns negócios
            irão desaparecer e não serão substituídos por outros sendo que a
            confiança dos consumidores irá voltar lentamente. Durante os
            períodos de estado de emergência existe a expectativa de algum
            crescimento por adaptação do negócio. Com este cenário,
            perspectiva-se uma recuperação total apenas no fim de 2021.""",
            "Description_EN": """Weak economic recovery and contained virus.
            With this scenario we will only have one lockdown period
            and there will be a weak economic recovery. Some businesses
            will disappear and will not be replaced by others as consumer
            confidence growth will return slowly. During lockdowns there will
            be some material growth due to business adaptation.
            With this scenario we will only see an economic recovery
            by the end of 2021."""
            },
        "Virus Contained + Mid Economic Recovery with Lockdown Growth": {
            "Lockdown_1": {
                "Lockdown Start": 0,
                "Lockdown Length": 10,
                "Initial Drop": 0.8,
                "Lockdown Death Ratio Intercept": 0.02,
                "Lockdown Death Ratio Slope": 0.02,
                "Lockdown Growth Intercept": 0,
                "Lockdown Growth Slope": 0.03
            },
            "Recovery_1": {
                "Recovery New Clients Intercept": 0.02,
                "Recovery New Clients Slope": 0.02,
                "Recovery Growth Intercept": 0,
                "Recovery Growth Slope": 0.01
            },
            "Description_PT": """Vírus contido e recuperação económica
            média com efeito ascendente.
            Neste cenário iremos ter apenas um período de estado de
            emergência e perspectiva-se uma média recuperação económica.
            Durante o período de estado de emergência existe a expectativa
            de algum crescimento por adaptação do negócio.
            A confiança dos consumidores irá demorar algum tempo a recuperar.
            Com este cenário, a economia deverá retornar ao normal no segundo
            trimestre de 2021.""",
            "Description_EN": """Virus contained and mid economic recovery with
            an ascending slope.
            With this scenario we will only have one lockdown period and
            there is an expectation of a mid economic recovery.
            During the lockdown period there will be some substantial
            business growth due to business adaptation.
            Consumer confidence will take some time to recover and with this
            scenario there will be a full recover by Q3 2021."""
            },
        }

    return scenarios


def read_industries() -> dict:
    '''
    Reads industry weights and outputs
    them in dictionary format
    '''
    industries = {
        "--general": [
            1
        ],
        "Airlines": [
            0.7
        ],
        "Oil and Gas": [
            1.2
        ],
        "Travel": [
            0.7
        ],
        "Insurance": [
            0.95
        ],
        "Banks": [
            1
        ],
        "Real Estate": [
            1
        ],
        "Automotive": [
            1
        ],
        "Fashion": [
            1
        ],
        "Business Services": [
            1
        ],
        "Materials": [
            1.1
        ],
        "Electricity": [
            1.2
        ],
        "Food and Beverages": [
            1.2
        ],
        "Retail": [
            1
        ],
        "Tech": [
            1
        ],
        "Pharmaceutical": [
            1.3
        ],
        "Telecommunications": [
            1.3
        ],
        "Food Retail": [
            1.5
        ],
        "Media": [
            1.5
        ],
        "Adhoc": [
            1.5
        ]
    }

    return industries
