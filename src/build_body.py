import pandas as pd
import plotly.graph_objects as go
import streamlit as st


def build_body(
    forecasts: pd.DataFrame
) -> None:
    '''
    Builds body of the streamlit app with a graph and a bar plot.

    Args:
        forecasts(pd.DataFrame): Pandas dataframe with forecasts.
    Returns:
        None
    '''

    # Rewrite index format - for graph and table in streamlit only
    plot_forecasts = forecasts.set_index('year_week')
    forecasts.drop(['year_week'], axis=1, inplace=True)

    # Draw line chart with forecasts - main anchor of streamlit body
    st.line_chart(plot_forecasts)

    # Group by month for table
    monthly_data = (forecasts.groupby(
        pd.Grouper(freq='M')
        )
        .sum()
        .round()
        )

    # Draw layout for interactive bar graph with plotly
    layout = go.Layout(
        plot_bgcolor='rgba(0,0,0,0)'
    )

    fig = go.Figure(data=[
        go.Bar(
            name='Adjusted Forecast',
            x=monthly_data.index,
            y=monthly_data.adjusted_forecast,
            marker_color='darkgreen'),
        go.Bar(
            name='Normal Forecast',
            x=monthly_data.index,
            y=monthly_data.normal_forecast - monthly_data.adjusted_forecast,
            marker_color='lightgreen',
            marker_line_color='black',
            opacity=0.2),
    ],
        layout=layout
    )

    fig.update_layout(barmode='stack')

    # Draw graph and table
    st.plotly_chart(fig)

    st.dataframe(plot_forecasts)
