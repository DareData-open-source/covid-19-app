import matplotlib.pyplot as plt
import pandas as pd
from PyPDF2 import PdfFileWriter, PdfFileReader, PdfFileMerger
from reportlab.lib.utils import ImageReader
import io
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from datetime import datetime
from src import input_parameters
import os

# Register font
pdfmetrics.registerFont(TTFont('Arial', 'Arial.ttf'))


def chunkstring(
    string: str,
    length: int
):
    '''
    Chunks string into pieces of size equal to length parameter.
    Args:
        string(str): Original text
        length(inst): The size of each chunk
    Returns:
        (list): List with string elements of size "length"

    '''
    return (string[0+i:length+i] for i in range(0, len(string), length))


def write_scenario_description(
    desc: str,
    size: int,
    y_axis_start: int,
    can
):
    '''
    Writes the description of the scenario into the canvas "can".
    Canvas is type of 2-D plane where the origin is the left bottom
    corner of a pdf sheet.
    The y-axis start sets the beginning of the description. Descriptions
    are written on the point 70 of X-axis. Every line has size "size"
    so that the description doesn't get of the PDF

    Args:
        desc(str): Description of the scenario
        size(int): Size of canvas limit
        y_axis_start(int): The Y-axis start value in the canvas
        can(reportlab.pdfgen.canvas): The canvas to write the pages
    Returns:
        None

    '''

    # Replace those empty lines
    desc = desc.replace('\n            ', ' ')

    # Split into chunks of size
    list_sentences = list(chunkstring(desc, size))

    # Check if a word has been cut and amend that
    for i, sent in enumerate(list_sentences):
        try:
            if list_sentences[i+1] != ' ':
                list_sentences[i] += list_sentences[i+1].split(' ')[0]
                list_sentences[i+1] = ' '.join(
                    list_sentences[i+1].split(' ')[1:]
                    )
        except IndexError:
            pass

        # Write each sentence spaced by 15 points in the canvas
        can.drawString(70, y_axis_start, list_sentences[i].strip())
        y_axis_start -= 15


def write_figure_page(
    dataframe: pd.DataFrame,
    monthly_data: pd.DataFrame,
    forecast: str
):
    '''
    Writes page with line graph
    and dataframe per scenario and
    dimension.

    Args:
        dataframe(pd.DataFrame): dataframe with original values
        monthly_data(pd.DataFrame): dataframe with aggregated data
        forecast(str): Name of the dimension to plot
    '''

    # Write Figure
    plt.figure(figsize=(10, 10))
    # Write Plot on subplot on figure
    ax = plt.subplot(211)
    ax.plot(dataframe)
    ax.set_title(
        'Sales forecast for dimension '+str(forecast) +
        " with and without COVID-19 impact adjustment.",
        fontsize=10
    )
    ax.legend([
        'Forecasted Sales',
        ("Forecasted sales with the adjustement " +
            "caused by the COVID19 crisis."),
        'Real Sales'
        ],
        loc='upper left',
        fontsize=8
    )
    plt.xticks(rotation=90, fontsize=8)

    header_colors = "wheat"

    # Write Table
    ax2 = plt.subplot(212)
    table = ax2.table(
        cellText=monthly_data.values,
        colLabels=monthly_data.columns,
        rowLabels=monthly_data.index,
        rowColours=[header_colors]*monthly_data.shape[0],
        colColours=[header_colors]*monthly_data.shape[1],
        cellLoc='center',
        rowLoc='center',
        loc='center',
        colWidths=[0.2, 0.2, 0.10, 0.20, 0.20]
    )

    # Set font size on table
    table.auto_set_font_size(False)
    table.set_fontsize(8)
    table.scale(1.5, 1.5)

    # Save this PDF
    ax2.axis('off')
    plt.tight_layout()
    plt.savefig('reports/'+forecast+'.pdf')
    plt.clf()


def write_intro_page(
    forecast: str,
    index: int,
    scenario_desc: str,
    company_name: str
):

    """
    Writes first page of PDF report

    Args:
        forecast(str): The dimension of the forecast
        index(int): Index of the forecast - helps to control the
        writing of the first page.
        scenario_desc(str): Description of the scenario
        can(reportlab.pdfgen.canvas): Scenario Canvas
    Returns:
        output(PdfFileWriter): Resulting pdf writer object

    """

    pdf_file = 'reports/'+forecast+'.pdf'

    # Open Scenario Writing PDF
    infile = PdfFileReader(pdf_file, 'rb')
    output = PdfFileWriter()

    packet = io.BytesIO()
    # Create a new PDF with Reportlab
    can = canvas.Canvas(packet, pagesize=letter)

    # If it is the first scenario we are writing, write a detailed cover page
    if index == 0:
        can.setFont('Helvetica-Bold', 20)
        can.drawString(70, 650, (
            "COVID19 Economic Recovery Report"))
        can.line(70, 645, 1600, 645)
        can.setFont('Helvetica-Bold', 8)
        can.drawString(70, 635, "Company: "+company_name)
        can.setFont('Arial', 10)
        can.drawString(70, 600, (
            "The following report describes the evolution of sales" +
            "according with different pandemic scenarios"
        ))
        can.drawString(70, 590, "previously studied.")
        can.drawString(70, 560, (
            "The impact can be seen graphically or through " +
            "the table that has the following columns:"
        ))
        can.setFont('Helvetica-Bold', 10)
        can.drawString(70, 540, (
            "- Sales forecast based on same-period of the year-before."
            )
        )
        can.drawString(70, 525, "- Adjusted sales based on COVID19 Scenario.")
        can.drawString(70, 510, "- Same-Period % change.")
        can.drawString(70, 495, (
            "- Unit change (according to measured unit)."
            )
        )

    can.setFont('Helvetica-Bold', 15)
    can.drawString(70, 400, (
        "Scenario Description:"
        )
    )
    can.line(70, 395, 300, 395)
    can.setFont('Arial', 10)

    write_scenario_description(
        scenario_desc,
        96,
        380,
        can
    )

    can.setFont('Helvetica-Bold', 15)
    can.drawString(70, 280, (
        "Studied Dimension:"
        )
    )
    can.setFont('Arial', 15)
    can.line(70, 275, 300, 275)
    can.drawString(70, 260, (
        forecast
    ))

    can.setFont('Arial', 8)
    can.drawString(420, 20, (
        "Report Date: " +
        str(datetime.now())
    ))

    # Read Daredata Logo and set it up on the front
    daredatalogo = ImageReader('assets/DareDataLogo.png')
    can.drawImage(
        daredatalogo,
        500,
        700,
        width=100,
        height=80,
        mask=None,
        preserveAspectRatio=True,
        anchor='c')

    # Read company logo and put it at the other corner of the page

    can.showPage()
    can.save()
    packet.seek(0)
    new_pdf = PdfFileReader(packet)

    output.addPage(new_pdf.getPage(0))
    output.addPage(infile.getPage(0))
    outputStream = open(pdf_file, "wb")
    output.write(outputStream)
    outputStream.close()

    return output


def write_excel_file(
    dataframe: pd.DataFrame,
    scenario: str
) -> None:
    """
    Writes excel file with forecast weekly data

    Args:
        dataframe(dict): dataframe to export.
        scenario(str): Scenario of dataframes.
    """
    dataframe.to_excel('reports/'+scenario+'.xlsx', sheet_name=scenario)


def export_data(
    forecasts_dim: dict,
    company_name: str
) -> None:
    """
    Writes PDF page per dimension
    If dimension is global only writes one page

    Args:
        forecasts_dim(dict): Dictionary with the dataframes for each dimension
        to write report.
        company_name(str): Name of the company that the report refers to
    Returns:
        (None)
    """

    # For each table in forecasts dim
    for index, forecast in enumerate(forecasts_dim):

        # Extract data and Scenario Desc
        dataframe = forecasts_dim[forecast]['data']
        scenario = forecasts_dim[forecast]['scenario']

        # Read Scenarios from sidebar
        scenarios_info = input_parameters.read_scenarios()

        # Get the Description from Scenario, Replacing the white spaces
        scenario_desc = (
            scenarios_info[scenario]['Description_EN'].replace(
                '\n            ',
                ' '
                )
        )

        # Drop Real Sales
        # dataframe.drop('Sales', axis=1, inplace=True)

        # Round predictions
        import numpy as np
        dataframe = dataframe.astype(float).apply(np.round)

        # Aggregate by month
        monthly_data = dataframe.groupby(
            [lambda x: x.year, lambda x: x.month]
            ).sum()

        # Calculate new data columns for report
        monthly_data["Change related to Same-Period Last year"] = (
            (
                monthly_data.adjusted_forecast
                -
                monthly_data.normal_forecast)
        )

        monthly_data["% change related to Same-Period Last year"] = (
            (
                monthly_data.adjusted_forecast
                -
                monthly_data.normal_forecast)
            /
            monthly_data.normal_forecast
        ).map(lambda x: "{0:.2f}%".format(x*100))

        # Rename columns to Portuguese for Report
        monthly_data.rename(
            columns=(
                {
                    'normal_forecast': 'Sales Forecast',
                    'adjusted_forecast': """Adjusted Forecast
        due to COVID-19""",
                    'Sales': """Real
         Sales""",
                    'Change related to Same-Period Last year': """Unit change
         same-period Last year""",
                    '% change related to Same-Period Last year': """% change
         same-period Last year""",
                    }
            ),
            inplace=True
        )

        # Write the Plot and Table First
        write_figure_page(
            dataframe,
            monthly_data,
            forecast
        )

        output = write_intro_page(forecast, index, scenario_desc, company_name)

        if index == 0:
            outputStream = open('reports/full_report.pdf', "wb")
            output.write(outputStream)
            outputStream.close()
        else:
            merger = PdfFileMerger()
            merger.append(PdfFileReader('reports/full_report.pdf', 'rb'))
            merger.append(PdfFileReader('reports/'+forecast+'.pdf', 'rb'))
            outputStream = open('reports/full_report.pdf', "wb")
            merger.write(outputStream)
            outputStream.close()

        write_excel_file(dataframe, scenario)


def concatenate_excel_files(
    path: str
):
    '''
    Concatenates every excel file in a folder
    and appends them into a single final file

    Args:
        path(str): Folder to look for the Excel Files
    '''

    files = os.listdir(path)
    # Subset XLSX Files
    files_xls = [f for f in files if f[-4:] == 'xlsx']

    data = {}
    for i, f in enumerate(files_xls):
        f_sales = pd.read_excel(path+'/'+f)
        data[f.split('.')[0]] = f_sales

    # Loop through every dictionary object and write it in a separate
    # sheet in the final file

    writer = pd.ExcelWriter(path+'/Scenarios_Data.xlsx')
    for key in data.keys():
        data[key].to_excel(writer, sheet_name=key, index=False)

    writer.save()
    writer.close()
