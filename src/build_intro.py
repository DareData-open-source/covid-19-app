import streamlit as st


def write_header() -> None:
    '''
    Writes streamlit header.
    Everything that is informative to the reader
    should be put here.

    Writes on streamlit app and does not return anything.
    '''

    def write_new_line():
        st.write('\n')

    # Links to the articles that are refered on the header
    l1 = "in-europe-and-around-the-world-governments-are-getting-tougher"
    l2 = "covid-19-economic-recovery-model-c587cb2fff64"
    l3 = "1cD82e4LuWe0lUrHlFCy9GBYMcJesbVRGy9zBlU9r0wM/edit"

    # Write title
    st.header('COVID19 - Economic Recovery Model Simulation')

    # Markdown intro text - link to articles and use
    # variables to respect flake8 linting
    st.subheader(
        "The following simulations are based on the following writings:"
    )
    st.markdown("")
    st.markdown(
        """[- Economist article]
        (https://www.economist.com/briefing/2020/03/19/"""+l1+')'
    )
    st.markdown(
        """[- Medium article]
        (https://medium.com/daredata-engineering/"""+l2+')'
    )
    st.markdown(
        """[- Technical paper]
        (https://docs.google.com/document/d/"""+l3+')'
    )
    st.markdown("")
    st.markdown("""The impact in the sales forecast is measured by several
    parameters anchored by the number of lockdowns. Each lockdown also
    generates a set of recovery parameters.""")
    st.markdown("""**Check the official documentation to know how
    to change the parameters for each phase.**""")
    st.subheader(
        "Select your sales data below:"
    )

    # Write new line - if we need more white space between header and
    # rest of script, increase the range
    for i in range(1):
        write_new_line()
