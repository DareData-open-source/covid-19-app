import streamlit as st
from src import (
    read_data,
    transform_data,
    build_forecast,
    build_intro,
    scenarios,
    build_body,
    export_data,
    export_data_en,
    sidebar
)


def build_parametrized_app():
    '''
    Builds streamlit app. This app is the streamlit where users can
    parametrize their data regarding scenarios.
    '''
    # Build intro
    build_intro.write_header()

    # Get company name from user on app-
    company_name = st.text_input('Name of Company:')

    # Read entry sales file - This is given by the streamlit dropdown.
    df = read_data.load_data()

    # Transform and group data
    df = transform_data.format_group_data(df)

    # Set growth rate
    growth_rate = sidebar.set_growth_rate(0)

    # Generate forecast based on sales - we are currently
    # using sixteen march onwards to generate a forecast
    # and are only mapping homologous period with a growth factor
    # associated
    normal_forecast = build_forecast.generate(
        df,
        '2019-03-16',
        '2020-03-01'
    )

    # Get current sales from the data
    current_sales = build_forecast.generate(
        df,
        '2020-03-02',
        '2021-02-28',
        forecast=False
    )

    # Writes markdown guiding the user on the streamlit app
    st.markdown("""**If you want to tweak the parameters yourself leave the
    the dropdown below as is. Otherwise, choose an industry below to model
    pre-saved scenarios: **""")

    # Scenario set understands if the user wants to parametrize
    # the scenario himself or if he wants to read a scenario
    # from the available pre-set ones
    lockdown_objects, recovery_objects, scenario = scenarios.scenario_set()

    # Set weight of the industry - currently these are a bit "arbitrary"
    industry_weight = scenarios.set_industry_weight()

    # Generate forecast
    forecasts = build_forecast.adjust_forecast(
        normal_forecast,
        lockdown_objects,
        recovery_objects,
        industry_weight,
        growth_rate
    )

    # Join Current Sales
    forecasts = transform_data.join_forecast_current(
        forecasts,
        current_sales
    )

    # Build forecast
    build_body.build_body(forecasts)

    # Initialize scenario Dimension
    forecasts_dim = {}
    forecasts_dim['Global'] = {}
    forecasts_dim['Global']['data'] = forecasts
    forecasts_dim['Global']['scenario'] = scenario

    # Select dropdown with language to export - currenty we support
    # Portuguese and English
    selected_language = st.selectbox(
        'Select the language of your export files:',
        ['English', 'Portuguese']
    )

    # Export button to get pdf and excel files
    if st.button('Export data to a PDF and Excel File'):
        if selected_language == 'English':
            export_data_en.export_data(forecasts_dim, company_name)
        else:
            export_data.export_data(forecasts_dim, company_name)

    # This appends the excel files into a single file
    if st.button('Append already exported excel files into one'):
        export_data.concatenate_excel_files('reports')
