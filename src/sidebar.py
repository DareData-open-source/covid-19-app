import streamlit as st
import recoverymodel as rm


def get_lockdown_numbers(default=1):
    lockdown_number = (st.sidebar.number_input(
        "Number of Lockdowns",
        0,
        100,
        value=default
        )
    )

    return lockdown_number


def lockdown_parameters(
    lockdown_number: int,
    start=0,
    length=8,
    initial_drop=0.8,
    death_rate_b0=0.0,
    death_rate_slope=0.0,
    lockdown_g_b0=0.0,
    lockdown_g_slope=0.0
):
    (
        st.sidebar.markdown
        (
            '**Lockdown Number '+str(lockdown_number)+' Parameters:**'
        )
    )

    start = st.sidebar.number_input(
        "Lockdown Week Start",
        0,
        100,
        value=start,
        key=lockdown_number
    )

    length = st.sidebar.number_input(
        "Lockdown Week Length",
        0,
        100,
        value=length,
        key=lockdown_number
    )

    initial_drop = st.sidebar.slider(
        'Choose percentage of initial drop: ',
        min_value=0.0,
        max_value=1.0,
        value=initial_drop,
        step=0.05,
        key=lockdown_number
    )

    death_rate_b0 = (
        st.sidebar.number_input(
            "Death Rate Intercept",
            0.0,
            1.0,
            value=death_rate_b0,
            key=lockdown_number
            )
    )

    death_rate_slope = (
        st.sidebar.number_input(
            "Death Rate Slope",
            0.0,
            1.0,
            value=death_rate_slope,
            key=lockdown_number
            )
    )

    lockdown_g_b0 = (
        st.sidebar.number_input(
            "Lockdown Growth Intercept",
            0.0,
            1.0,
            value=lockdown_g_b0,
            key=lockdown_number
            )
    )

    lockdown_g_slope = (
        st.sidebar.number_input(
            "Lockdown Growth Slope",
            0.0,
            1.0,
            value=lockdown_g_slope,
            key=lockdown_number
            )
    )

    return (
        start, length, initial_drop,
        death_rate_b0, death_rate_slope,
        lockdown_g_b0, lockdown_g_slope
    )


def recovery_parameters(
    recovery_number: int,
    client_recovery_b0=0.0,
    client_recovery_slope=0.0,
    market_appetite_b0=0.0,
    market_appetite_slope=0.0
):
    (
        st.sidebar.markdown
        (
            '**Recovery Number '+str(recovery_number)+' Parameters:**'
        )
    )

    client_recovery_b0 = (
        st.sidebar.number_input(
            "Client Recovery Intercept",
            0.0,
            1.0,
            value=client_recovery_b0,
            key=recovery_number
            )
    )

    client_recovery_slope = (
        st.sidebar.number_input(
            "Client Recovery Slope",
            0.0,
            1.0,
            value=client_recovery_slope,
            key=recovery_number
            )
    )

    market_appetite_b0 = (
        st.sidebar.number_input(
            "Market Appetite Intercept",
            0.0,
            1.0,
            value=market_appetite_b0,
            key=recovery_number
            )
    )

    market_appetite_slope = (
        st.sidebar.number_input(
            "Market Appetite Slope",
            0.0,
            1.0,
            value=market_appetite_slope,
            key=recovery_number
            )
    )

    return (
        client_recovery_b0, client_recovery_slope,
        market_appetite_b0, market_appetite_slope
    )


def scenario_update_params(
    index: int,
    lockdown: dict,
    recovery: dict
):
    '''
    Updates parameters on sidebar based
    on a specific loaded scenario
    '''

    p1, p2, p3, p4, p5, p6, p7 = lockdown_parameters(
        index,
        lockdown['Lockdown Start'],
        lockdown['Lockdown Length'],
        lockdown['Initial Drop'],
        float(lockdown['Lockdown Death Ratio Intercept']),
        float(lockdown['Lockdown Death Ratio Slope']),
        float(lockdown['Lockdown Growth Intercept']),
        float(lockdown['Lockdown Growth Slope'])
    )

    r1, r2, r3, r4 = recovery_parameters(
        index,
        float(recovery['Recovery New Clients Intercept']),
        float(recovery['Recovery New Clients Slope']),
        float(recovery['Recovery Growth Intercept']),
        float(recovery['Recovery Growth Slope'])
    )

    return (
        p1,
        p2,
        p3,
        rm.generic_linear_func(p4, p5),
        rm.generic_linear_func(p6, p7),
        rm.generic_linear_func(r1, r2),
        rm.generic_linear_func(r3, r4)
    )


def set_growth_rate(
    index_dim: int
):
    '''
    Sets growth rate for forecast

    Returns:
        growth_rate(np.float): Growth factor to apply to current sales
    '''

    growth_rate = (
        st.sidebar.number_input(
            "Set Growth Rate (%) to Apply:",
            0.0,
            1.0,
            value=0.0,
            key=index_dim
            )
    )

    return growth_rate
