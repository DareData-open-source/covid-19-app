import recoverymodel as rm
import pandas as pd
import numpy as np


def generate(
    df: pd.DataFrame,
    start_date: str,
    end_date: str,
    forecast=True
) -> pd.DataFrame:

    '''
    Builds projections for the year ahead
    using same-period last year.
    Define the period to project forward with start_date
    and end_date - this depends on the forecast Flag. If we are
    not asking for a forecast it just maps the current sales since
    the start of the pandemic.

    Args:
        df(pd.DataFrame): Dataframe with original sales.
        start_date(str): Start date of the sales period.
        end_data(str): End date of the sales period.
        forecast(bool): Forecast flag.
    Returns:
        fcast.set_index('Date')(pd.DataFrame): Output table with
        sales (forecast or real).
    '''

    mask = (df['Date'] > start_date) & (df['Date'] <= end_date)

    # add a year to current sales - this is the available forecast for now
    fcast = df.loc[mask]

    # If forecast, add a year
    if forecast:
        fcast['Date'] = fcast['Date'] + pd.DateOffset(years=1)

    # Create Auxiliary column year_week based on date
    fcast['year_week'] = fcast['Date'].dt.strftime('%Y-%U')

    return fcast.set_index('Date')


def adjust_forecast(
    normal_forecast: pd.Series,
    lockdown_objects: dict,
    recovery_objects: dict,
    industry_weight: float,
    growth_rate: float
) -> pd.DataFrame:
    '''
    Returns forecasted sales according to lockdown and recovery scenario
    parameters.

    Args:
        normal_forecast(pd.Series): Original forecast of sales.
        lockdown_objects(dict): Dictionary with each lockdown parameters.
        recovery_objects(dict): Dictionary with each recovery parameters.
        industry_weight(float): Industry weight to apply.
        growth_rate(float): Growth rate to apply to original forecast.
    Returns:
        forecasts(pd.DataFrame): Adjusted sales forecast.
    '''

    # Apply growth factor.
    normal_sales = normal_forecast.Sales*(1+growth_rate)

    # Create recovery model object.
    recovery = rm.RecoveryModel(
        # Normal Sales Forecast (Business as Usual)
        normal_forecast=normal_sales.map(float).tolist(),
        # Lockdown Objects that characterize each lockdown phase
        lockdowns=lockdown_objects,
        # Recovery Objects that characterize each recovery phase
        recoveries=recovery_objects
        )

    # Adjust forecast.
    adjusted_forecast = recovery.model_recovery()

    # Create table with normal and adjusted forecast.
    forecasts = pd.DataFrame(
        {
            'normal_forecast': recovery._normal_forecast,
            'adjusted_forecast': adjusted_forecast,
        },
        index=normal_sales.index,
    )

    # Apply industry weight.

    forecasts['adjusted_forecast'] = (
        forecasts['adjusted_forecast'] * industry_weight[0]
    )

    # If our forecast if bigger than original forecast, ignore the
    # the adjustment.
    forecasts['adjusted_forecast'] = np.where(
        forecasts.normal_forecast <= forecasts.adjusted_forecast,
        forecasts.normal_forecast,
        forecasts.adjusted_forecast
    )

    forecasts['year_week'] = normal_forecast.year_week

    return forecasts
