import pandas as pd


def format_group_data(
    df: pd.DataFrame
) -> pd.DataFrame:
    '''
    Formats and groups timeseries
    as weekly data.
    '''

    # Format date as datetime
    df['Date'] = pd.to_datetime(df.Date, dayfirst=True)
    # Remove weird "normal" stuff from sales
    df['Sales'] = (
        df['Sales']
        .astype(str)
        .apply(lambda x: x.replace('$', ''))
        .apply(lambda x: x.replace('€', ''))
        .apply(lambda x: x.replace(' ', ''))
        .astype(float)
    )
    # Group data by weekly intervals
    df['Date'] = pd.to_datetime(df['Date']) - pd.to_timedelta(6, unit='d')

    df = (df.groupby(
        [pd.Grouper(key='Date', freq='W-MON')])['Sales']
        .sum()
        .reset_index()
        .sort_values('Date')
    ).round()

    return df


def join_forecast_current(
    df1: pd.DataFrame,
    df2: pd.DataFrame
) -> pd.DataFrame:

    joined_df = df1.merge(df2, on='year_week', how='left')
    joined_df.set_index(df1.index, inplace=True)

    return joined_df
