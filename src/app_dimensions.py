import streamlit as st
from src import (
    read_data,
    build_intro,
    scenarios,
    build_forecast,
    transform_data,
    build_body,
    export_data,
    export_data_en,
    sidebar
)


def build_app_dimensions():
    '''
    Builds streamlit dimensions app.
    Dimensions app enables the user to create multiple COVID scenarios.
    Example reports that can be created:
    - Several scenarios for one time series of sales;
    - Apply scenarios to each product.
    - Apply scenarios to each region.

    This function is triggered according to a button that is clicked
    by the user in the streamlit app.
    '''
    # Build app intro
    build_intro.write_header()

    # Ask for company name in the streamlit app
    company_name = st.text_input('Name of Company:')

    # Read entry sales file with multiple dimensions
    # This is given by the streamlit dropdown
    # Each dimension is represented by a sheet
    dimensions_data = read_data.load_dimension_data()

    # Selector for dimension on sidebar
    st.sidebar.markdown('**Select scenario for each dimension:**')

    # Create a dictionary that will contain the forecast
    # and scenario description
    forecasts_dim = {}

    # Iterate through each dimension in the excel file
    # Each dimension = excel sheet.
    for index, dimension in enumerate(dimensions_data):

        # Each element in forecast dim dict will also be a dictionary
        forecasts_dim[dimension] = {}

        # Write the dimension name on streamlit sidebar
        st.sidebar.markdown('**'+dimension+'**')

        # Obtain growth rates to apply
        # if you want to apply any growth rate to the forecast
        growth_rate = sidebar.set_growth_rate(index)

        # Transform data
        data = transform_data.format_group_data(
            dimensions_data[dimension]
        )

        # Extract normal forecast for the dates between
        # 16 March year n until 1 march n+1
        # TO-DO: Make this a parameter from app
        normal_forecast = build_forecast.generate(
            data,
            '2019-03-16',
            '2020-03-01'
        )

        # Get current sales that have occurred since the beginning of the
        # pandemic
        current_sales = build_forecast.generate(
            data,
            '2020-03-02',
            '2021-02-28',
            forecast=False
        )

        # Obtain lockdown and recovery objects for each dimension.
        # In the parametrized app there is no room for tweaking the parameters
        # you only fit several scenarios with pre-set scenarios.
        # We will keep doing this to avoid having an huge ammount of parameters
        # for the user to tweak, at the cost of being a counter-productive
        # approach.
        lockdown_objects, recovery_objects, scenario = scenarios.scenario_set(
            dimensions=True,
            iterator=index
        )

        # Produce forecast based on our recovery models
        forecasts = build_forecast.adjust_forecast(
            normal_forecast,
            lockdown_objects,
            recovery_objects,
            [1],
            growth_rate
        )

        # Join forecast and current sales
        forecasts = transform_data.join_forecast_current(
            forecasts,
            current_sales
        )

        # Build body of the streamlit app
        build_body.build_body(
            forecasts
        )

        # Write data and scenario description in the dictionaty
        forecasts_dim[dimension]['data'] = forecasts
        forecasts_dim[dimension]['scenario'] = scenario

    # Select language of files to export - Supports portuguese and english
    selected_language = st.selectbox(
        'Select the language of your export files:',
        ['English', 'Portuguese']
    )

    # Button to enable user to explore data to a PDF and Excel file
    if st.button('Export data to a PDF and Excel File'):
        if selected_language == 'English':
            export_data_en.export_data(forecasts_dim, company_name)
        else:
            export_data.export_data(forecasts_dim, company_name)
