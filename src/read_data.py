import pandas as pd
import streamlit as st
import os


def select_file(folder_path='../data/covid_project') -> os.path:
    '''
    Lists files coming from the data folder
    in the streamlit app - the resulting
    selection from the user will be the file
    used to do the forecast
    '''
    filenames = os.listdir(folder_path)
    selected_filename = st.selectbox(
        'Select one of the available files: ',
        filenames)
    return os.path.join(folder_path, selected_filename)


def load_data() -> pd.DataFrame:
    '''
    Loads file with the original data.
    Returns a dataframe.
    Other raises an exception in case on unexpected file format.
    '''
    fname = select_file()

    ext = fname.split('.')[-1]
    if ext == 'csv':
        df = pd.read_csv(fname)
    elif ext in ['xlsx', 'xls']:
        df = pd.read_excel(fname, converters={0: str})
    else:
        raise Exception('Please upload a valid file format.')

    if df.shape[1] == 2:
        df.columns = ['Date', 'Sales']
    else:
        raise Exception('''Please upload a file with two columns,
    a date column and a sales column.''')

    return df


def load_dimension_data() -> pd.DataFrame:
    '''
    Loads different files according to the
    dimension studied. This will be a multi-data
    '''

    fname = select_file()

    # Read file extension
    ext = fname.split('.')[-1]

    dfs = {}

    if ext in ['xlsx', 'xls']:

        xl = pd.ExcelFile(fname)

        for sheet in xl.sheet_names:
            df = pd.read_excel(
                fname,
                converters={0: str},
                sheet_name=sheet
            )

            if df.shape[1] == 2:
                df.columns = ['Date', 'Sales']
            else:
                raise Exception('''Please upload a file with two columns,
            a date column and a sales column.''')
            dfs[sheet] = df
    else:
        raise Exception('''Please upload a valid file format.''')

    return dfs
